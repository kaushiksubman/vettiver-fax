var logger = require('../../server/logger');
var log = logger.child({
  module: 'Image'
});
var jimp = require('jimp');
var streamToBuffer = require('stream-to-buffer');
var imageType = require('image-type');
var path = require('path');
module.exports = function (Image) {

  Image.observe('after save', function (ctx, next) {
    if (!ctx.instance) {
      return next();
    }
    if (!ctx.isNewInstance) {
      return next();
    }
    if (ctx.instance.dynamicUrl) {
      return next();
    }
    ctx.instance.dynamicUrl =
      Image.app.get('baseUrl') + Image.app.get('restApiRoot') +
      '/images/' + ctx.instance.id +
      '/renditions?sz={{size}}&redirect=true&access_token={{accessToken}}';
    ctx.instance.save(function (err, updatedInstance) {
      if (err) {
        log.error(err, 'error setting dynamic url of image');
      }
      next(err, updatedInstance);
    });
  });


  Image.prototype.syncReferences = function (cb) {
    var self = this;
	  Image.app.models.Campaign.updateAll({
	    '_picture.id': self.id
	  }, {
	    $set: {
	      '_picture.renditions': self.renditions
	    }
	  }, cb);
  };
  Image.prototype.resize = function (size, callback) {
    var image = this;
    if (!image.filename || !image.container) {
      var e = new Error('Missing meta data');
      e.details =
        'Specified Image is third party and does not support resizing';
      return callback(e);
    }
    var downloadStream = Image.app.models.ImageContainer.downloadStream(
      image.container, image.filename, {});
    streamToBuffer(downloadStream, function (err, buffer) {
      if (err) {
        return callback(err);
      }
      var mimeType = imageType(buffer).mime;
      jimp.read(buffer, function (err, img) {
        if (err) {
          return callback(err);
        }
        if (!img) {
          err = new Error('JIMP.read img not defined');
          return callback(err);
        }
        var imgWidth = img.bitmap.width;
        var imgHeight = img.bitmap.height;
        var aspectRatio = imgWidth / imgHeight;
        var sizeWidth = size;
        var sizeHeight = size / aspectRatio;
        img.resize(sizeWidth, sizeHeight);
        var pathObj = path.parse(image.filename);
        pathObj.name = pathObj.name + '-' + size;
        pathObj.base = pathObj.name + pathObj.ext;
        var newFileName = path.format(pathObj);
        var uploadStream = Image.app.models.ImageContainer.uploadStream(
          image.container, newFileName, {
            acl: 'public-read',
            contentType: mimeType
          });
        img.getBuffer(mimeType, function (err, buffer) {
          uploadStream.on('success', function (file) {
            image.renditions = image.renditions || {};
            var imageCDNUrl = Image.app.get('imageCDNUrl');
            var imageUrl = file.location;
            if (imageCDNUrl) {
              imageUrl =
                imageCDNUrl + '/' + decodeURIComponent(file.name);
            }
            var updateObj = {};
            updateObj['renditions.' + size] = imageUrl;
            image.updateAttributes(updateObj, function (err, updatedImage) {
              if (err) {
                return callback(err, null);
              }
              callback(err, updatedImage.renditions[size] || imageUrl);
            });
          });
          uploadStream.write(buffer);
          uploadStream.end();
          uploadStream.on('error', function (err) {
            callback(err);
          });
        });
      });
    });
  };

  Image.prototype.renditions = function (sz, redirect, res, cb) {
    var self = this;
    if (sz > 1200) {
      var e = new Error('Size Mismatch');
      e.details = 'Maximum Size parameter 870';
      return cb(e);
    }
    function respond(err, redirectUrl) {
      if (!redirect) {
        return cb(err, {redirect: redirectUrl});
      }
      res.redirect(redirectUrl);
    }

    if (this.renditions && this.renditions[sz]) {
      this.syncReferences(function (err) {
        if (err) {
          return respond(err, null);
        }
        return respond(null, self.renditions[sz]);
      });
      return;
    }
    this.resize(sz, function (err, result) {
      respond(err, result);
    });
  };

  Image.remoteMethod('renditions', {
    accepts: [
      {arg: 'sz', type: 'number', http: {source: 'query'}},
      {arg: 'redirect', type: 'boolean', http: {source: 'query'}},
      {arg: 'res', type: 'object', http: {source: 'res'}}
    ],
    returns: {arg: 'data', type: 'object', root: true},
    http: {path: '/renditions', verb: 'get'},
    isStatic: false
  });
}
;
