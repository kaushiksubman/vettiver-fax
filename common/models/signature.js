var Phaxio = require('phaxio');
var request = require('request');
var fs = require('fs');
var logger = require('../../server/logger');
var log = logger.child({
  module: 'Signature'
});

module.exports = function(Signature) {
	Signature.validatesPresenceOf('campaignId');
  Signature.validatesUniquenessOf('email');
	Signature.observe('after save', function (ctx, next) {
		if (!ctx.instance) {
			return next();
		}
		if (!ctx.isNewInstance) {
			return next();
		}
		function sendFax(faxObject, next) {
			var phaxioConfig = Signature.app.get('phaxio');
	    var phaxio = new Phaxio(phaxioConfig.apiKey, phaxioConfig.secret);
	    phaxio.sendFax(faxObject, function (err, data) {
	      if (err) {
	      	log.error({error: err}, 'Error sending fax');
	      	ctx.instance.error = true;
	      	ctx.instance.errorObject = err;
	      	return ctx.instance.save(next);
	      }
	      if (data && data.message) {
	      	ctx.instance.statusMessage = data.message;
	    	}
	      if (data && data.success) {
	      	ctx.instance.faxStatus = 'complete';
	      }
	      ctx.instance.save(next);
	    });
		}
		Signature.app.models.Campaign.findById(ctx.instance.campaignId,
			function (err, campaign) {
				if (err) {
					return next(err);
				}
				if (!campaign) {
					var e = new Error('Campaign not found');
					e.details = 'The given id was not found in the database';
					return next(e);
				}
				var to = campaign.faxNumbers;
				var faxTo = campaign.faxTo + ',\n\n';
				var headerText = campaign.faxBody;
				var stringData = headerText + '\nSigned by: ' + ctx.instance.firstName + ' ' +
					ctx.instance.lastName + '\nE-mail: ' + ctx.instance.email +
					'\nPostal Code: ' + ctx.instance.postalCode +
					'\nCountry: ' + ctx.instance.country;
				var faxObject = {
					to: to,
					string_data: stringData	
				};
				if (campaign._faxFile) {
					var faxFile = campaign._faxFile;
					var fileNameArray = faxFile.title.split('/');
					var fileName = fileNameArray[fileNameArray.length - 1];
					faxObject['filenames'] = [fileName];
					var file = request(faxFile.url).pipe(fs.createWriteStream(fileName));
					file.on('finish', function () {
						sendFax(faxObject, next);
					});
					return;
				}
				sendFax(faxObject, next);
			});
	});
};
