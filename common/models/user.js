var util = require('util');
var uid = require('uid');
module.exports = function(User) {
  User.prototype.uploadImage = function (req, res, next) {
    var self = this;
    // noinspection JSUnusedGlobalSymbols
    var uploadOptions = {
      container: User.app.dataSources.imageStorage.settings.bucketName,
      acl: 'public-read',
      allowedContentTypes: ['image/png', 'image/jpeg'],
      maxFileSize: 5 * 1024 * 1024,
      getFilename: function (fileInfo) {
        var date = new Date();
        return util.format('%d/%d/%d/%s', date.getUTCMonth(),
          date.getUTCDate(),
          date.getUTCFullYear(),
          (uid(12) + '-' + encodeURIComponent(fileInfo.name)).toLowerCase());
      }
    };

    User.app.models.ImageContainer.upload(req, res, uploadOptions,
      function (err, result) {
        if (err) {
          return next(err);
        }
        if (!result.files.hasOwnProperty('file')) {
          var e = new Error('No file');
          e.details = 'No file uploaded. `file` parameter can\'t be blank';
          return res.json(e);
        }
        var fileInfo = result.files.file[0];
        var imageCDNUrl = User.app.get('imageCDNUrl');
        var imageUrl = imageCDNUrl || 'http://' + fileInfo.container +
          '.s3.amazonaws.com';
        imageUrl += '/' + fileInfo.name;
        self.images.create({
          filename: fileInfo.name,
          container: fileInfo.container,
          url: imageUrl
        }, function (err, image) {
          if (err) {
            return next(err);
          }
          res.json(image);
          res.send();
        });
      });
  };
  User.remoteMethod('uploadImage', {
    accepts: [{
      arg: 'req',
      type: 'object',
      http: {
        source: 'req'
      }
    }, {
      arg: 'res',
      type: 'object',
      http: {
        source: 'res'
      }
    }],
    returns: {
      arg: 'result',
      type: 'object',
      root: true
    },
    http: {
      path: '/images/upload',
      verb: 'post'
    },
    isStatic: false
  });
  User.prototype.uploadFile = function (req, res, next) {
    var self = this;
    // noinspection JSUnusedGlobalSymbols
    var uploadOptions = {
      container: User.app.dataSources.fileStorage.settings.bucketName,
      acl: 'public-read',
      allowedContentTypes: ['application/pdf',
      	'image/png',
      	'image/jpeg',
      	'application/msword',
      	'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'image/tiff',
        'image/x-tiff',
        'text/plain',
        'text/html',
        'application/vnd.oasis.opendocument.text'
      ],
      maxFileSize: 10 * 1024 * 1024,
      getFilename: function (fileInfo) {
        var date = new Date();
        return util.format('%d/%d/%d/%s', date.getUTCMonth(),
          date.getUTCDate(),
          date.getUTCFullYear(),
          (uid(12) + '-' + encodeURIComponent(fileInfo.name)).toLowerCase());
      }
    };

    User.app.models.FileContainer.upload(req, res, uploadOptions,
      function (err, result) {
        if (err) {
          return next(err);
        }
        if (!result.files.hasOwnProperty('file')) {
          var e = new Error('No file');
          e.details = 'No file uploaded. `file` parameter can\'t be blank';
          return next(e);
        }
        var fileInfo = result.files.file[0];
        var fileCDNUrl = User.app.get('fileCDNUrl');
        var fileUrl = fileCDNUrl || 'http://' + fileInfo.container +
          '.s3.amazonaws.com';
        fileUrl += '/' + fileInfo.name;
        self.files.create({
          filename: fileInfo.name,
          container: fileInfo.container,
          url: fileUrl
        }, function (err, file) {
          if (err) {
            return next(err);
          }
          res.json(file);
          res.send();
        });
      });
  };
  User.remoteMethod('uploadFile', {
    accepts: [{
      arg: 'req',
      type: 'object',
      http: {
        source: 'req'
      }
    }, {
      arg: 'res',
      type: 'object',
      http: {
        source: 'res'
      }
    }],
    returns: {
      arg: 'result',
      type: 'object',
      root: true
    },
    http: {
      path: '/files/upload',
      verb: 'post'
    },
    isStatic: false
  });
};
