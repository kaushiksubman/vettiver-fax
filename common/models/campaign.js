var async = require('async');
var logger = require('../../server/logger');
var log = logger.child({
  module: 'Campaign'
});
module.exports = function(Campaign) {
	//Campaign.validatesPresenceOf('_faxFile'); - fax file  not needed
	Campaign.observe('before save', function (ctx, cb) {
		if (!ctx.instance) {
			return cb();
		}
		var tasks = [];
		tasks.push(function (next) {
		  var pic = ctx.instance._picture;
		  if (!pic || !pic.id || pic.processed) {
		    return next();
		  }

		  Campaign.app.models.Image.findById(pic.id, function (err, image) {
		    if (err) {
		      return next(err);
		    }
		    if (!image) {
		      var e = new Error('Image not found');
		      return next(e);
		    }
		    var picture = ctx.instance.picture.build({
		      title: pic.title || image.filename
		    });
		    picture.image(image);
		    picture.processed = true;
		    next();
		  });
		});

		tasks.push(function (next) {
		  var fax = ctx.instance._faxFile;
		  if (!fax || !fax.id || fax.processed) {
		    return next();
		  }

		  Campaign.app.models.File.findById(fax.id, function (err, file) {
		    if (err) {
		      return next(err);
		    }
		    if (!file) {
		      var e = new Error('File not found');
		      return next(e);
		    }
		    var faxFile = ctx.instance.faxFile.build({
		      title: fax.title || file.filename
		    });
		    faxFile.file(file);
		    faxFile.processed = true;
		    next();
		  });
		});
		async.parallel(tasks, function (err) {
			if (err) {
				return cb(err);
			}
			cb();
		});
	});
  Campaign.prototype.linkPicture = function (data, cb) {
    var self = this;
    Campaign.app.models.Image.findById(data.id, function (err, image) {
      if (err) {
        return cb(err);
      }
      if (!image) {
        err = new Error('Image not found');
        err.details = 'Image with specified Id does\'t exist';
        return cb(err);
      }
      self._picture = data;
      self._picture.processed = false;
      self.save(function (err, updatedCampaign) {
        cb(null, updatedCampaign._picture);
      });
    });
  };
  Campaign.remoteMethod('linkPicture', {
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      path: '/picture/link',
      verb: 'post'
    },
    isStatic: false
  });
  Campaign.prototype.linkFile = function (data, cb) {
    var self = this;
    Campaign.app.models.File.findById(data.id, function (err, file) {
      if (err) {
        return cb(err);
      }
      if (!file) {
        err = new Error('File not found');
        err.details = 'File with specified Id does\'t exist';
        return cb(err);
      }
      self._faxFile = data;
      self._faxFile.processed = false;
      self.save(function (err, updatedCampaign) {
        cb(null, updatedCampaign._faxFile);
      });
    });
  };
  Campaign.remoteMethod('linkFile', {
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body'
      }
    }],
    returns: {
      arg: 'data',
      type: 'object',
      root: true
    },
    http: {
      path: '/faxFile/link',
      verb: 'post'
    },
    isStatic: false
  });
  Campaign.afterRemote('prototype.__create__signatures',
    function (ctx, modelInstances, next) {
    	if (!ctx.instance) {
    		return next();
    	}
    	var campaign = ctx.instance;
    	campaign.updateAttributes({
    		$inc: {
    			signatureCount: 1
    		}
    	}, function (err, updatedCampaign) {
    		if (err) {
    			return next(err);
    		}
    		next();
    	});
    });
};
