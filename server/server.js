var loopback = require('loopback');
var boot = require('loopback-boot');
var bodyParser = require('body-parser');
var logger = require('./logger');
var log = logger.child({module: 'server'});
var Phaxio = require('phaxio');

var app = module.exports = loopback();

app.use(loopback.context());
app.use(loopback.token());
app.use(bodyParser.json({limit: '5mb'}));

app.use(function setCurrentUser(req, res, next) {
  // handler for /users/me route
  if (req.url.indexOf('/users/me') > -1) {
    // this is not applicable if access token is absent
    if (!req.accessToken) {
      var e = new AuthError(AuthError.AUTHENTICATION_REQUIRED);
      res.status(401);
      return res.send({
        error: e
      });
    }
    req.url = req.url.replace('/users/me',
      '/users/' + req.accessToken.userId.toString());
    req.params.id = req.accessToken.userId.toString();
  }
  if (!req.accessToken) {
    return next();
  }
  app.models.user.findById(req.accessToken.userId, function (err, user) {
    if (err) {
      return next(err);
    }
    if (!user) {
      var e = new RequestError(RequestError.USER_NOT_FOUND);
      e.details = 'User not Found. User account is probably deleted';
      res.status(404);
      return res.json({error: e});
    }
    var loopbackContext = loopback.getCurrentContext();
    if (loopbackContext) {
      loopbackContext.set('currentUser', user);
      loopbackContext.set('accessToken', req.accessToken);
    }
    next();
  });
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) {
    throw err;
  }
  app.get('/testFax', function (req, res) {
    var phaxioConfig = app.get('phaxio');
    var phaxio = new Phaxio(phaxioConfig.apiKey, phaxioConfig.secret);
    phaxio.sendFax({
      to: '13165555555',
      string_data: 'Faxing from Node.js',
      string_data_type: 'text'
    }, function (err, data) {
      if (err) {
        return res.status(500).send(err);
      }
      res.status(200).send(data);
    });
  });

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
    process.on('uncaughtException', function (err) {
      // Log the error
      log.fatal(err, 'Uncaught Exception in server.js');
      process.exit();
    });
  }
  app.emit('loaded');
});
process.on('SIGINT', function () {
  process.exit();
});
