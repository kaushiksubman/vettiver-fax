var mongoUri = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL ||
  'mongodb://vettiver:vettiver123@ds023425.mlab.com:23425/vettiver-fax';
module.exports = {
  mongodb: {
    url: mongoUri
  }
};