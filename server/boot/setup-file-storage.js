var dataSources = require('../datasources.json');
var async = require('async');
var logger = require('../logger');
module.exports = function (app) {
  var log = logger.child({module: 'setup-storage'});
  app.models.FileContainer.getContainer(dataSources.fileStorage.bucketName,
    function (err, container) {
      if (err) {
        log.error(err,
          'error fetching container: ' + dataSources.fileStorage.bucketName);
      }
      if ((err && false) || !container) {
        app.models.container.createContainer(dataSources.fileStorage.bucketName,
          function (error) {
            if (error) {
              log.error(error, 'error creating container');
            }
          });
      }
    });
};