var dataSources = require('../datasources.json');
var async = require('async');
var logger = require('../logger');
module.exports = function (app) {
  var log = logger.child({module: 'setup-storage'});
  app.models.ImageContainer.getContainer(dataSources.imageStorage.bucketName,
    function (err, container) {
      if (err) {
        log.error(err,
          'error fetching container: ' + dataSources.imageStorage.bucketName);
      }
      if ((err && false) || !container) {
        app.models.container.createContainer(dataSources.imageStorage.bucketName,
          function (error) {
            if (error) {
              log.error(error, 'error creating container');
            }
          });
      }
    });
};