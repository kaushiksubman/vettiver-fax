var dataSources = require('../datasources.json');
var async = require('async');
var logger = require('../logger');
module.exports = function (app, done) {
  var log = logger.child({module: 'seed-data'});
  var User = app.models.user;
  var Application = app.models.application;
  var Role = app.models.Role;
  var RoleMapping = app.models.RoleMapping;
  createAdmin();

  function createAdmin() {
    var admin = app.get('admin');
    User.create(admin, function (err, createdAdmin) {
      // DO NOT handle error, err is expected if users are already created.
      User.findOne({
        where: {
          username: admin.username
        }
      }, function (err, foundAdmin) {
        if (err) {
          log.error(err, 'error finding admin User');
          done(err); // let's not start the server if there's an error
        }
        var tasks = [];
        tasks.push(function (next) {
          var appDetails = {
            userId: foundAdmin.id.toString(),
            name: app.get('appName'),
            description: 'Vettiver Fax Application',
          };
          updateOrCreateApp(appDetails, function (err, appModel) {
            if (err) {
              next(err);
            }
            app.set('applicationId', appModel.id);
            next();
          });
        });

        tasks.push(function (next) {
          // create the admin role
          Role.findOrCreate({
            name: 'admin'
          }, function (err, role) {
            if (err) {
              log.error(err, 'error creating admin role');
            }
            app.set('adminRole', role);
            RoleMapping.findOrCreate({
              roleId: role.id,
              principalType: RoleMapping.USER,
              principalId: foundAdmin.id.toString()
            }, function (err, roleMapping) {
              if (err) {
                log.error(err,
                  'error finding admin role mapping');
              }
              next(err, roleMapping);
            });
          });
        });
        async.parallel(tasks, function (err) {
          // Tasks completed
          if (err) {
            log.error(err, 'error running async.parallel');
            done(err);
          }
          done();
        });
      });
    });
  }

  function updateOrCreateApp(appDetails, cb) {
    Application.findOne({
        where: {name: appDetails.name}
      },
      function (err, result) {
        if (err) {
          return cb(err);
        }
        if (result) {
          result.updateAttributes(appDetails, cb);
        } else {
          return registerApp(appDetails, cb);
        }
      });
  }

  function registerApp(appDetails, cb) {
    Application.register(
      appDetails.userId,
      appDetails.name,
      {
        description: appDetails.description
      },
      function (err, app) {
        if (err) {
          return cb(err);
        }
        return cb(null, app);
      }
    );
  }
};
